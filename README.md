# ClickerBot

Programmable clicker bot (for Windows only, now "ported" to VS)

(And by ported to VS I mean, copy&paste into a VS project)

---

Do you want to play a clicker game but you are tired of clicking? With this programmable
bot, you can now win at any clicker game by doing literally NOTHING!

The bot loads a script in text format using an assembler-like language with the instructions,
and then proceed to execute said instructions.

You can compile this using Visual Studio! (Or port it to MinGW easily, because that's where it came from)

---

## Usage

> ClickerBot.exe \<script location\>

Where \<script location\> is the location of the file containing the bot script.

---

## Scripting

The scripting is done using an assembly-like language that has no name. Basically you set the position
of the mouse cursor wherever you want in the screen, then perform clicks or drags & releases at the
mouse position. It's really simple to use, though it requires you to know the location on the screen,
in absolute coordinates, of whatever you want to click.

The program is written in a text file, that you can call whatever you like. Then, you just load this
script into the program. It's that simple.

The following instructions can be used:

### mov \<x\> \<y\>

Moves the cursor to the coordinates specified.

### moh \<x\> \<y\>

Moves the cursor to the coordinates specified, holding the left mouse button. Can be used to
drag stuff or swipe the screen.

### clk

Performs a click (press and release of the left mouse button) at the current mouse position.

### hld

Performs a press on the left mouse button, and holds it.

### rel

Releases the left mouse button.

### del \<milliseconds\>

Delays the next instruction for the specified amount of milliseconds. 1000ms = 1 second.

### lop (number of iterations)

Makes the program loop from this point through the next set of instructions, until a 'lnx' instruction
is found, for whatever number of iterations you specify. The number of iterations parameter
is optional, if you don't set it, it will loop forever (until F12 is pressed).

### lnx

Sets the point in which the program should loop. When the program reaches this point, the program counter
will be set to the previous lop instruction.

### IMPORTANT

Each instruction ends with a semicolon (;) in C style. Example programs can be found in the 'patbot.txt'
and 'patbot2.txt' files.

---

## About the usage of C code in MSVC++

This code was directly ported from C code that was compilable with Mingw, and it hasn't been properly ported
to MSVC++ (and probably won't ever be properly ported, because I'm too lazy). I am well aware that this isn't
how it should be done, but for now, if it works, it works.

Also this makes it easy to compile the program for mingw if you'd prefer to do so, as the code can be directly
compiled in mingw.

## About bugs and issues

I made this in 2 hours at 2AM in the morning. Please be aware that this isn't going to be 'rocket science', things
might go wrong if you use it the wrong way and bugs might exist.