// ClickerBot.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Windows.h>

int pdx = 0;
int pdy = 0;
int vexit = 0;
int loop = 0;
int looppos = 0;
char hold = 0;
char isHolding = 0;
int pc = 0;
//char doLoop = 0;

char** inst;
unsigned int instructions = 0L;

void increaseProgram()
{
	char** instCopy = (char**)calloc(instructions, sizeof(char**));
	memcpy(instCopy, inst, instructions * sizeof(char**));
	free(inst);
	inst = (char**)calloc(++instructions, sizeof(int**));
	memcpy(inst, instCopy, instructions * sizeof(char**));
	free(instCopy);
	return;
}

char* newInstruction(char* instruction)
{
	increaseProgram();
	inst[instructions - 1] = instruction;
	return instruction;
}

void initializeProgram()
{
	inst = (char**)calloc(1, sizeof(int*));
}

void freeAll()
{
	int p = 0;
	if (instructions < 0 || !inst)
		return;

	for (p = 0; p < instructions; p++)
	{
			free(inst[p]);
			instructions--;
	}

	free(inst);
	inst = 0;
}

int mouseMove(long dx, long dy)
{
	pdx = dx;
	pdy = dy;
	INPUT mInput = { 0 };
	mInput.mi.dx = dx * (65536 / GetSystemMetrics(SM_CXSCREEN));
	mInput.mi.dy = dy * (65536 / GetSystemMetrics(SM_CYSCREEN));
	mInput.mi.mouseData = 0x0;
	mInput.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;
	return SendInput(1, &mInput, sizeof(INPUT));
}

int mouseMoveHold(long dx, long dy)
{
	hold = 1;
	pdx = dx;
	pdy = dy;
	INPUT mInput = { 0 };
	mInput.mi.dx = dx * (65536 / GetSystemMetrics(SM_CXSCREEN));
	mInput.mi.dy = dy * (65536 / GetSystemMetrics(SM_CYSCREEN));
	mInput.mi.mouseData = 0x0;
	mInput.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE | MOUSEEVENTF_LEFTDOWN;
	return SendInput(1, &mInput, sizeof(INPUT));
}

int mouseClick()
{
	INPUT mInput = { 0 };
	mInput.mi.dx = pdx * (65536 / GetSystemMetrics(SM_CXSCREEN));;
	mInput.mi.dy = pdy * (65536 / GetSystemMetrics(SM_CYSCREEN));;
	mInput.mi.mouseData = 0x0;
	mInput.mi.dwFlags = MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_ABSOLUTE | 0x4000;
	int suc = SendInput(1, &mInput, sizeof(INPUT));
	if (suc < 1)
		return suc;

	mInput.mi.dx = 0;
	mInput.mi.dy = 0;
	mInput.mi.mouseData = 0x0;
	mInput.mi.dwFlags = MOUSEEVENTF_LEFTUP;
	return SendInput(1, &mInput, sizeof(INPUT));
}

int mouseHold()
{
	hold = 1;
	INPUT mInput = { 0 };
	mInput.mi.dx = pdx * (65536 / GetSystemMetrics(SM_CXSCREEN));;
	mInput.mi.dy = pdy * (65536 / GetSystemMetrics(SM_CYSCREEN));;
	mInput.mi.mouseData = 0x0;
	mInput.mi.dwFlags = MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_ABSOLUTE | 0x4000;
	int suc = SendInput(1, &mInput, sizeof(INPUT));
	return suc;
}

int mouseRelease()
{
	hold = 0;
	INPUT mInput = { 0 };
	mInput.mi.dx = 0;
	mInput.mi.dy = 0;
	mInput.mi.mouseData = 0x0;
	mInput.mi.dwFlags = MOUSEEVENTF_LEFTUP;
	return SendInput(1, &mInput, sizeof(INPUT));
}

int delay(int ms)
{
	Sleep(ms);
	return 1;
}

int executeInstruction(char* inst)
{
	vexit = GetKeyState(VK_F12);
	char cmd[4] = { 0x0 };
	char pam1[6] = { 0x0 };
	char pam2[6] = { 0x0 };
	memcpy(cmd, inst, 3);
	if (strcmp(cmd, "lnx") == 0)
	{
		if (loop > 0 || loop < 0)
		{
			printf("Loop %i pc %i\n", loop, pc);
			pc = looppos;
			if(loop > 0)
				loop--;
		}
	}
	if (strcmp(cmd, "lop") == 0)
	{
		int loops = -1;
		int cur = 4;
		int a = 0;
		char sep = 0;

		while (1)
		{
			if (inst[cur] == ',')
			{
				sep++;
				a = 0;
				cur++;
				continue;
			}
			if (inst[cur] == ';' || inst[cur] == '\n')
				break;

			if (inst[cur] == ' ')
			{
				cur++;
				continue;
			}

			if (sep == 0)
			{
				pam1[a] = inst[cur];
				a++;
			}
			else if (sep == 1)
			{
				pam2[a] = inst[cur];
				a++;
			}
			cur++;
		}

		if (a > 0)
			loops = atoi(pam1);
		else
			loops = -1;

		loop = loops;
		looppos = pc+1;
	}
	if (strcmp(cmd, "mov") == 0 || strcmp(cmd, "moh") == 0)
	{
		
		int cur = 4;
		int a = 0;
		char sep = 0;

		if (strcmp(cmd, "moh") == 0)
			isHolding = 1;
		else
			isHolding = 0;

		while (1)
		{
			if (inst[cur] == ',')
			{
				sep++;
				a = 0;
				cur++;
				continue;
			}
			if (inst[cur] == ';')
				break;

			if (inst[cur] == ' ')
			{
				cur++;
				continue;
			}

			if (sep == 0)
			{
				pam1[a] = inst[cur];
				a++;
			}
			else if (sep == 1)
			{
				pam2[a] = inst[cur];
				a++;
			}
			cur++;
		}
		int px_ = atoi(pam1);
		int py_ = atoi(pam2);

		if (!isHolding)
		{
			printf("Move cursor to %i %i\n", px_, py_);
			if (!mouseMove(px_, py_))
				printf("Error\n");
		}
		else {
			printf("Move cursor holding click to %i %i\n", px_, py_);
			if (!mouseMoveHold(px_, py_))
				printf("Error\n");
		}

		isHolding = 0;
	}
	if (strcmp(cmd, "del") == 0)
	{
		int cur = 4;
		int a = 0;
		char sep = 0;
		while (1)
		{
			if (inst[cur] == ';')
				break;

			if (inst[cur] == ' ' || inst[cur] == ',')
			{
				cur++;
				continue;
			}
			else {

				pam1[a] = inst[cur];
				a++;
				cur++;
			}
		}
		int ds = atoi(pam1);
		printf("Delay %i ms\n", ds);
		delay(ds);
	}
	if (strcmp(cmd, "clk") == 0)
	{
		printf("Click at current mouse position.\n");
		if (!mouseClick())
			printf("Error\n");
	}
	if (strcmp(cmd, "hld") == 0)
	{
		printf("Click hold start at current mouse position.\n");
		if (!mouseHold())
			printf("Error\n");
	}
	if (strcmp(cmd, "rel") == 0)
	{
		printf("Click released at current mouse position.\n");
		if (!mouseRelease())
			printf("Error\n");
	}
	return 0;
}

void executeProgram()
{
	//pc = program counter
	//loop = times to loop (0 if none)
	//looppos = start of the loop (pc goes to looppos once it reaches a lop if loop > 0)
	//inst = char** with the program
	//instructions = number of instructions

	printf("Instructions: %i\n", instructions);
	while (pc < instructions)
	{
		vexit = GetKeyState(VK_F12);

		if ((vexit & 0x8000))
		{
			printf("Program break (F12). Exiting...\n");
			return;
		}

		executeInstruction(inst[pc]);
		pc++;
	}
}

int loadScript(char* path)
{
	FILE* scr = fopen(path, "r");
	if (!scr)
		return 0;

	char str[256];
	char* inst;

	while (fgets(str, sizeof(str), scr) && !(vexit & 0x8000))
	{
		inst = (char*)calloc(256, 1);
		strcpy(inst, str);
		newInstruction(inst);
		memset(str, 0x0, 256);
	}

	fclose(scr);
	return 1;
}


int main(int argc, char** argv)
{
	//if(args < 2)
			//return 0;
	printf("Ready to load %s.\n", argv[1]);
	//while (!(vexit & 0x8000))
	//{
		if(!loadScript(argv[1]))
		{
			printf("Couldn't load file %s. Exiting.\n", argv[1]);
			return 1;
		}
		printf("Ready to execute %s. While executing, press F12 to exit. Press any key when ready...\n", argv[1]);
		getchar();
		executeProgram();
		mouseRelease();
		freeAll();
	//}
	return 0;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
